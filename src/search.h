/*
 *	search.h
 *  This file is part of NotepadQt
 *
 *  Copyright (C) 2019, Stian Halstensen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SEARCH_H
#define SEARCH_H

#include <QWidget>

class NotepadQt;
class QLineEdit;

class Search : public QWidget
{
    Q_OBJECT

public:
    explicit Search(QWidget *parent = nullptr, NotepadQt *nqt = nullptr);
    void toggleSearch();
    void find();    /* Public to be accessible from eventeater F3 */

private:
    NotepadQt *notepadqt   { nullptr };
    QLineEdit *searchText  { nullptr };
    QLineEdit *replaceText { nullptr };

private slots:

    void findPrev();
    void replace();
    void replaceAll();
    void cancel();
};

#endif // SEARCH_H
