/*
 *	fileexplorer.cpp
 *  This file is part of NotepadQt
 *
 *  Copyright (C) 2019, Stian Halstensen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "fileexplorer.h"
#include "editor.h"


FileExplorer::FileExplorer()
{
    /* Bind signal to slot */
    connect(this, SIGNAL(itemClicked(QListWidgetItem*)),
            this, SLOT(itemClicked(QListWidgetItem*)));

    /* Start hidden */
    this->hide();
}


void FileExplorer::toggleExplorer(Editor *e, const QString &s)
{
    if (!e)
        return;
    editor = e;

    if (s == "")
        curDir = QDir::current();
    else
        curDir.setPath(s);

    this->isVisible() ? this->hide() : this->show();

    if (this->isVisible())
        refreshList();
}


void FileExplorer::refreshList()
{
    this->clear(); /* All items will be permanently deleted. */

    /* Grab files and folders in current dir... */
    curDir.setNameFilters(QStringList() << "*");
    QStringList list = curDir.entryList();

    /* Populate a new list */
    for (const auto &x : list)
        new QListWidgetItem(x, this);

    this->setFocus();
}


void FileExplorer::itemSelect()
{
    if (!this->currentItem())   /* Abort if object lookup failed */
        return;

    curDir.cd(this->currentItem()->text());
    QFileInfo fi(curDir.absoluteFilePath(this->currentItem()->text()));

    if (fi.isFile())
    {
        this->hide();
        editor->setFocus();
        editor->fileOpen(curDir.absoluteFilePath(this->currentItem()->text()));
        this->clear();
    }
    else
    {
        refreshList();
    }
}


void FileExplorer::itemClicked(QListWidgetItem *)
{
    itemSelect();
}
