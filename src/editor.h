/*
 *	editor.h
 *  This file is part of NotepadQt
 *
 *  Copyright (C) 2019, Stian Halstensen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef EDITOR_H
#define EDITOR_H

#include <QPlainTextEdit>
#include <QObject>

class QPaintEvent;
class QResizeEvent;
class QSize;
class QWidget;
class Syntaxhl;
class Lexer;

class Editor : public QPlainTextEdit
{
    Q_OBJECT

public:
    enum Template   /* Insert snippets */
    {
        Template_For,
        Template_Switch
    };

    enum Direction  /* Cursor movement */
    {
        Direction_Up,
        Direction_Down,
        Direction_Left,
        Direction_Right,
        Direction_Beginning,
        Direction_End
    };

    Editor(QWidget *parent = nullptr);
    void cursorMove(Direction);
    void cursorStep(Direction);
    /* fileClose = fileNew() */
    void fileCloseWithoutSave();
    void fileNew();
    void fileOpen(const QString &f = "");
    void filePrint();
    void fileSave();
    void fileSaveAs();
    void indent();
    void initSyntaxHl();
    void insertTemplate(Template);
    void killRectangle();
    void lineNumberAreaPaintEvent(QPaintEvent *event);
    void lineDuplicate();
    void lineDelete();
    void removeWhitespace();
    void sortSelection();
    void suggestionInsert();
    void suggestionSelect();
    void swapDefImp(const QString&, const QString&);
    void toLower();
    void toUpper();
    void toggleBlockComment();
    void toggleLineComment();
    int  lineNumberAreaWidth();

    QString filepath;       /* file name, including path */
    QString path;           /* absolute path. without file name */
    QString filename;       /* filename incl extension */
    QString suffix;         /* file extension */

    QString searchString;
    QString inputBuffer;    /* used with tab-suggestions */
    bool    openCalled;
    bool    savingRequired;

    Lexer   *lexer { nullptr };

protected:
    void resizeEvent(QResizeEvent*) override;
#ifdef Q_OS_WIN /* Req for Win10 to handle drops... */
    void dropEvent(QDropEvent *event) override;
#endif

private:
    void resetEditor();
    QWidget  *lineNumberArea { nullptr };
    Syntaxhl *syntaxhl       { nullptr };

private slots:
    void updateLineNumberAreaWidth(int newBlockCount);
    void highlightCurrentLine();
    void updateLineNumberArea(const QRect&, int);
    void getSelection(std::vector<QString>*);
};


class LineNumberArea : public QWidget
{
    Q_OBJECT

public:
    LineNumberArea(Editor *ed) :
        QWidget(ed),
        editor{ed}
    {
        //
    }

    QSize sizeHint() const override
    {
        return QSize(editor->lineNumberAreaWidth(), 0);
    }

protected:
    void paintEvent(QPaintEvent *event) override
    {
        editor->lineNumberAreaPaintEvent(event);
    }

private:
    Editor *editor;
};

#endif  // EDITOR_H
