/*
 *	lexer.cpp
 *  This file is part of NotepadQt
 *
 *  Copyright (C) 2019, Stian Halstensen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "lexer.h"

#include <QFile>        // File i/o
#include <QDebug>

#define SYNTAX_CPP ":/keywords/syntax_cpp.txt" // Temp...


Lexer::Lexer(const QString &file) :
    editorfile          { file },
    lastSearchWord      { "" },
    numCurrentSearchWord{ 0 },
    lexedKeyword        { false }

{
    if (file == "") return;
    timer_begin = std::chrono::high_resolution_clock::now();
    keywords = new QStringList;
    lexfile();
}


Lexer::~Lexer()
{
    delete keywords;
}

void Lexer::lexBuffer(const QString &s)             /* Run-time */
{
    // TODO: Handle other types....

    /* Check if word is keyword*/
    bool keyword = check_keyword(s, keywords);

    /* If keyword found, wait for next string */
    if (keyword)
    {
        lexedKeyword = true;
        return;
    }

    /* If no keyword is registered - return */
    if (!lexedKeyword)
        return;

    /* Format string - remove non-wanted chars */
    QString formStr;
    std::string line { s.toStdString()};
    for (size_t i{0}; i<line.length(); ++i)
    {
        if (std::isalpha(line[i])
                || std::isdigit(line[i])
                || line[i] == '_')
        {
            formStr.push_back(line[i]);
        }
    }

    /* Decide struct-slot. check if word exists in list
     * if false add new word */

    if (lastLexedKeyword == "enum")
    {
        if (!data.enums.contains(formStr, Qt::CaseSensitive))
            data.enums.push_back(formStr);
    }
    else if (s.contains('('))
    {
        if (!data.func.contains(formStr, Qt::CaseSensitive))
            data.func.push_back(formStr);
    }
    else
    {
        if (!data.var.contains(formStr, Qt::CaseSensitive))
            data.var.push_back(formStr);
    }

    lexedKeyword = false;
    print_result();
}


QString Lexer::getSuggestion(QString searchword)    /* Run-time */
{
    /* If suggestion-base changed, generate a new list
       and reset values, else continue traversing stored data */
    if (searchword != lastSearchWord)
    {
        filteredSearchResult.clear();
        filteredSearchResult = data.func.filter(searchword);
        filteredSearchResult.append(data.var.filter(searchword));
        // TODO: Handle other struct-lists
        filteredSearchResult.sort();
        lastSearchWord = searchword;
        numCurrentSearchWord = 0;
    }

    /* If there's no match return searchword */
    if (filteredSearchResult.empty())
        return searchword;

    /* If there's only one alt return this */
    if (filteredSearchResult.size() == 1)
        return filteredSearchResult[0];

    if (numCurrentSearchWord >= filteredSearchResult.size())
        numCurrentSearchWord = 0;

    // TODO: Remove "non-wanted" words from the list...
    int wordnum{numCurrentSearchWord};
    QChar a, b;
    for (int i{wordnum};i<filteredSearchResult.size();++i)
    {
        int j{0};
        do
        {
            a = filteredSearchResult[i].at(j);
            b = searchword[j];
            j++;
        } while (a == b
                 && j < filteredSearchResult[i].length()-1
                 && j < searchword.length()-1);

        if (j == searchword.length()-1)
            break;
        else
            wordnum++;
    }

    numCurrentSearchWord++;

    return (wordnum > filteredSearchResult.size()-1) ?
                searchword : filteredSearchResult[wordnum];
}


inline bool Lexer::check_keyword(const QString &s, QStringList *kw)
{
    for (const auto &x: *kw)
    {
        if (x == s)
        {
            data.keywords++;
            lastLexedKeyword = QString(x);
            return true;
        }
    }
    return false;
}


inline void Lexer::lexfile()
{
    // TODO: Optimize
    // Remove all QString - std::string conversation

    /* Get cpp keywords */
    read_keyfile();
    if (!keywords)
        return;

    /* Handle supplied file */
    QFile file(editorfile);
    if (!file.open(QIODevice::ReadOnly | QFile::Text))
        return;

    bool isKeyword{false};
    std::string c;  // current str
    std::string p;  // prev str

    while (!file.atEnd())
    {
        data.iterations++;
        std::string line { file.readLine().toStdString() };
        for (size_t i{0}; i<line.length();++i)
        {
            if (std::isalpha(line[i])
                    || std::isdigit(line[i])
                    || line[i] == '_'
                    || line[i] == '#'
                    || line[i] == '"'
                    || line[i] == '.')
            {
                c.push_back(line[i]);
            }
            else
            {
                if (c == "")
                {
                    isKeyword = false;
                    continue;
                }

                if (c[0] == '\"' && isKeyword)
                {
                    c.erase(0,1);
                    if (c == "")
                        continue;
                    c.erase(c.length()-1,1);
                }

                if (isKeyword && !std::isdigit(c[0]))
                {
                    if (p == "enum")
                        data.enums.push_back(QString::fromUtf8(c.c_str()));
                    else if (line[i] == '(')
                        data.func.push_back(QString::fromUtf8(c.c_str()));
                    else if (p == "#include")
                        data.includes.push_back(QString::fromUtf8(c.c_str()));
                    else
                        data.var.push_back(QString::fromUtf8(c.c_str()));

                    isKeyword = false;
                    p.clear();
                }
                else
                {
                    isKeyword = check_keyword(QString().fromStdString(c), keywords);
                    p = c;
                }
                c.clear();
            }
        }
    }
    file.close();
#ifndef QT_NO_DEBUG
    auto timer_end { std::chrono::high_resolution_clock::now() };
    qDebug() << "Lexer completed in: "
             << std::chrono::duration_cast<std::chrono::milliseconds>(
                    timer_end - timer_begin).count() << "ms";
    //print_result();
#endif
}


inline void Lexer::print_result()
{
    qDebug() << "Iterations: " << data.iterations;
    qDebug() << "Keywords:   " << data.keywords;
    qDebug() << "Variables:  " << data.var;
    qDebug() << "Enums:      " << data.enums;
    qDebug() << "Func:       " << data.func;
    qDebug() << "Includes:   " << data.includes;
}


inline void Lexer::read_keyfile()
{
    QFile file(SYNTAX_CPP);
    if (!file.open(QIODevice::ReadOnly | QFile::Text))
        return;

    while (!file.atEnd())
        *keywords << QString(file.readLine().simplified());

    file.close();
}
