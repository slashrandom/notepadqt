/*
 *	eventeater.cpp
 *  This file is part of NotepadQt
 *
 *  Copyright (C) 2019, Stian Halstensen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "eventeater.h"

#include <QEvent>
#include <QFileInfo>
#include <QDebug>
#include <QKeyEvent>
#include <QLabel>
#include <QMimeData>
#include <QString>
#include <QTextCursor>

#include "editor.h"
#include "fileexplorer.h"
#include "lexer.h"
#include "notepadqt.h"
#include "search.h"


EventEater::EventEater(NotepadQt *nqt) :
    modeModal { false },
    notepadqt { nqt }
{
    //
}


bool EventEater::eventFilter(QObject *obj, QEvent *ev)
{
    if (ev->type() == QEvent::FocusIn)
    {
        if (obj->objectName() == "Edit1" || obj->objectName() == "Edit2")
        {
            activeEditor = static_cast<Editor*>(obj);
            notepadqt->activeEditor = activeEditor;
        }
        return false;
    }

    if (ev->type() == QEvent::KeyPress)
    {
        QKeyEvent *keyEvent { dynamic_cast<QKeyEvent*>(ev) };

        if (keyEvent->modifiers() == Qt::NoModifier)
        {
            if (modeModal)
                return modalHandler(keyEvent);
            else
                return noModifier(keyEvent);
        }

        else if (keyEvent->modifiers() == Qt::ControlModifier)
        {
            return ctrlModifier(keyEvent);
        }

        else if (keyEvent->modifiers() == Qt::AltModifier)
        {
            return altModifier(keyEvent);
        }

        else if (keyEvent->modifiers() == Qt::ShiftModifier)
        {
            return shiftModifier(keyEvent);
        }

        else
        {
            return false;
        }
    }

    /* Drag and drop files into editor */
    // TODO: Verify functionality for Mac.
    if (ev->type() == QEvent::DragEnter)
    {
        QString e = static_cast<Editor*>(obj)->objectName();
        if (e == "Edit1")
            dragdrop = editor[0];
        if (e == "Edit2")
            dragdrop = editor[1];
        ev->setAccepted(true);
    }

    /*
     * QEvent::Drop is not triggered on MS_WIN, so custom
     * dropEvent(QDropEvent *event) override are handled
     * inside the Editor class
     */
    if (ev->type() == QEvent::Drop)
    {
        if (!dragdrop)
            return false;
        QDropEvent *qde = static_cast<QDropEvent*>(ev);
        QList<QUrl> ql = qde->mimeData()->urls();
        QString s = ql.at(0).path();
        if (!s.isEmpty())
        {
            dragdrop->fileNew();
            dragdrop->fileOpen(s);
        }
    }
    /* Everything else is passed through */
    return false;
}


bool EventEater::modalHandler(QKeyEvent* keyEvent)
{
    switch (keyEvent->key())
    {
        case Qt::Key_Return:
        {
            if (explorer->isVisible())
            {
                explorer->itemSelect();
            }
        } break;

        case Qt::Key_Escape:
        {
            if (explorer->isVisible())
            {
                explorer->hide();
                activeEditor->setFocus();
            }
        } break;

        case Modal_Commandlister:
        {
            // TODO: Auto-generate from bindings-list
            QLabel *cmdLister = new QLabel;
            cmdLister->setText(
                        "\n"
                        "MODAL_COMMANDLISTER:\t Qt::Key_H\n"
                        "MODAL_COMMENTINSERT:\t Qt::Key_Y\n"
                        "MODAL_COMMENTREMOVE:\t Qt::Key_Y\n"
                        "MODAL_COPY:\t\t\t Qt::Key_C\n"
                        "MODAL_COPYLINE:\t\t Qt::Key_F\n"
                        "MODAL_CURSOR_DOWN:\t\t Qt::Key_S\n"
                        "MODAL_CURSOR_LEFT:\t\t Qt::Key_A\n"
                        "MODAL_CURSOR_RIGHT:\t\t Qt::Key_D\n"
                        "MODAL_CURSOR_UP:\t\t Qt::Key_W\n"
                        "MODAL_CUT:\t\t\t Qt::Key_X\n"
                        "MODAL_CUTLINE:\t\t\t Qt::Key_Z\n"
                        "MODAL_EXIT:\t\t\t Qt::Key_0 (zero)\n"
                        "MODAL_GOENDOFLINE:\t\t Qt::Key_E\n"
                        "MODAL_GOSTARTOFLINE:\t\t Qt::Key_Q\n"
                        "MODAL_INSERTTODO:\t\t Qt::Key_T\n"
                        "MODAL_KILLBUFFER:\t\t Qt::Key_K\n"
                        "MODAL_KILLRECT:\t\t Qt::Key_R\n"
                        "MODAL_LISTBUFFERS:\t\t Qt::Key_B\n"
                        "MODAL_MOVEENDOFBUFFER:\t Qt::Key_End\n"
                        "MODAL_MOVESTARTOFBUFF:\t Qt::Key_Home\n"
                        "MODAL_OPEN_FILE:\t Qt::O\n"
                        "MODAL_PASTE:\t\t\t Qt::Key_V\n"
                        "MODAL_SETMARK:\t\t Qt::Key_Space\n"
                        "MODAL_SORT:\t\t Qt::Key_M\n"
                        "MODAL_TEXT_TOLOWER:\t\t Qt::L\n"
                        "MODAL_TEXT_TOUPPER:\t\t Qt::U\n"
                        "\n");
            cmdLister->show();
        } break;

        case Modal_Commentinsert:
            break;

            //case Modal_Commentremove:
            //  break;

        case Modal_Copy:
            activeEditor->copy();
            break;

        case Modal_Copyline:
            break;

        case Modal_Cursor_down:
            activeEditor->cursorMove(Editor::Direction::Direction_Down);
            break;

        case Modal_Cursor_left:
            activeEditor->cursorMove(Editor::Direction::Direction_Left);
            break;

        case Modal_Cursor_right:
            activeEditor->cursorMove(Editor::Direction::Direction_Right);
            break;

        case Modal_Cursor_up:
            activeEditor->cursorMove(Editor::Direction::Direction_Up);
            break;

        case Modal_Cut:
            activeEditor->cut();
            break;

        case Modal_Cutline:
            break;

        case Modal_Exit:
            notepadqt->close();
            break;

        case Modal_Goendofline:
            activeEditor->cursorStep(Editor::Direction::Direction_Right);
            break;

        case Modal_Gostartofline:
            activeEditor->cursorStep(Editor::Direction::Direction_Left);
            break;

        case Modal_Inserttodo:
            activeEditor->insertPlainText("// TODO(): ");
            break;

        case Modal_Killbuffer:          /* HIDE ... */
        {
            // Get non-active buffer
            int e{1};
            int s{1};
            if (activeEditor->objectName() == "Edit2")
            {
                e = 0;
                s = 0;
            }

            (editor[e]->isVisible()) ?
                        editor[e]->hide() : editor[e]->show();
            (status[s]->isVisible()) ?
                        status[s]->hide() : status[s]->show();
        } break;

        case Modal_Killrect:
            activeEditor->killRectangle();
            break;

        case Modal_Listbuffers:
            break;

        case Modal_Moveendofbuffer:
            activeEditor->cursorStep(Editor::Direction::Direction_End);
            break;

        case Modal_Movestartofbuff:
            activeEditor->cursorStep(Editor::Direction::Direction_Beginning);
            break;

        case Modal_Open_file:
        {
            //explorer->editor = activeEditor;
            explorer->toggleExplorer(activeEditor, activeEditor->path);
        } break;

        case Modal_Paste:
            activeEditor->paste();
            break;

        case Modal_Setmark:
            break;

        case Modal_Sort:
            activeEditor->sortSelection();
            break;

        case Modal_Text_tolower:
            activeEditor->toLower();
            break;

        case Modal_Text_toupper:
            activeEditor->toUpper();
            break;

        default:
            return false;
    }
    return true;
}


bool EventEater::noModifier(QKeyEvent *keyEvent)
{
    switch (keyEvent->key())
    {
        case Qt::Key_Space:     /*  */
        {
            if (activeEditor->lexer)
                activeEditor->lexer->lexBuffer(activeEditor->inputBuffer);
            activeEditor->inputBuffer.clear();
        } return false;

        case Qt::Key_Backspace: /*  */
        {
            if (activeEditor->inputBuffer != "")
            {
                if (activeEditor->textCursor().hasSelection())
                    activeEditor->inputBuffer.clear();
                else
                    activeEditor->inputBuffer.chop(1);
            }
        } return false;

        case Qt::Key_Tab:       /*  */
        {
            if (activeEditor->inputBuffer == "")
                activeEditor->indent();
            else
                activeEditor->suggestionInsert();
        } break;

        case Qt::Key_Return:    /*  */
        {
            if (explorer->isVisible())
            {
                explorer->itemSelect();
                return true;
            }

            if (activeEditor->textCursor().hasSelection())
            {
                activeEditor->suggestionSelect();
            }
            else if (activeEditor->inputBuffer != "")
            {
                if (activeEditor->lexer)
                    activeEditor->lexer->lexBuffer(activeEditor->inputBuffer);
                activeEditor->inputBuffer.clear();
                return false;
            }
            else
                return false;
        } break;

        case Qt::Key_Escape:
        {
            if (explorer->isVisible())
            {
                explorer->hide();
                activeEditor->setFocus();
            }
        } break;

        case Qt::Key_F1:    /* INSERT TEMPLATE: FOR */
        {
            activeEditor->insertTemplate(Editor::Template_For);
        } break;

        case Qt::Key_F2:    /* INSERT TEMPLATE: SWITCH*/
        {
            activeEditor->insertTemplate(Editor::Template_Switch);
        } break;

        case Qt::Key_F3:    /* SEARCH NEXT */
        {
            if (search->isVisible())
                search->find();
        } break;

        case Qt::Key_F4:    /* SWAPDEFIMP */
        {
            /* Swap reference and check for available editor */
            int e{1};
            (activeEditor->objectName() == "Edit2") ? e = 0 : e = 1;

            /* Pass string current filename to wanted editor */
            if (editor[e]->filename == "" && editor[e]->isVisible())
            {
                editor[e]->swapDefImp(activeEditor->path,
                                      activeEditor->filename);
            }
            else
            {
                editor[((e == 1) ? 0 : 1)]->swapDefImp(activeEditor->path,
                                                       activeEditor->filename);
            }
        } break;

        case Qt::Key_F5:    /* DUMPTOOTHERBUFFER */
        {
            /* Swap reference and check for available editor */
            int e{0};
            (activeEditor->objectName() == "Edit2") ? e = 1 : e = 0;
            QString text{ editor[e]->toPlainText() };
            editor[((e == 1) ? 0 : 1)]->setFocus();
            editor[((e == 1) ? 0 : 1)]->setPlainText(text);
        } break;

        case Qt::Key_F6:
        {

        } break;

        case Qt::Key_F10:
        {
            activeEditor->removeWhitespace();

        } break;

        case Qt::Key_F11:
        {
            activeEditor->killRectangle();

        } break;

            // Add a mark-function

        case Qt::Key_F12:   /* SHOWHIDEEDITOR */
        {
            // Get non-active buffer
            int e{1};
            int s{1};
            if (activeEditor->objectName() == "Edit2")
            {
                e = 0;
                s = 0;
            }

            (editor[e]->isVisible()) ?
                        editor[e]->hide() : editor[e]->show();
            (status[s]->isVisible()) ?
                        status[s]->hide() : status[s]->show();
        } break;

        case Qt::Key_Left:  /* ARROW KEYS */
        case Qt::Key_Right: /* Clears the buffer */
        case Qt::Key_Up:
        case Qt::Key_Down:
        {
            if (activeEditor->lexer)
                activeEditor->lexer->lexBuffer(activeEditor->inputBuffer);
            activeEditor->inputBuffer.clear();
        } return false;

        default:
        {
            activeEditor->inputBuffer.append(keyEvent->text());
            qDebug() << activeEditor->inputBuffer;
            return false;
        }
    }
    return true;
}


bool EventEater::altModifier(QKeyEvent *keyEvent)
{
    if (activeEditor->inputBuffer != "")
        activeEditor->inputBuffer.clear();

    switch (keyEvent->key())
    {
        case Qt::Key_Left:
        {
            activeEditor->cursorStep(Editor::Direction::Direction_Left);
        } break;

        case Qt::Key_Right:
        {
            activeEditor->cursorStep(Editor::Direction::Direction_Right);
        } break;

        case Qt::Key_Up:
        {
            activeEditor->cursorStep(Editor::Direction::Direction_Up);
        } break;

        case Qt::Key_Down:
        {
            activeEditor->cursorStep(Editor::Direction::Direction_Down);
        } break;

        case Qt::Key_7:
        {
            activeEditor->insertPlainText("{}");
        } break;

        case Qt::Key_8:
        {
            activeEditor->insertPlainText("[]");
        } break;

        default:
        {
            return false;
        }
    }
    return true;
}


bool EventEater::ctrlModifier(QKeyEvent *keyEvent)
{
    if (activeEditor->inputBuffer != "")
        activeEditor->inputBuffer.clear();

    switch (keyEvent->key())
    {
        case Qt::Key_Return:
        {
            int e{0};
            (activeEditor->objectName() == "Edit2") ? e = 1 : e = 0;

            /* Select other buffer */
            (e == 0) ? (e = 1) : (e = 0);
            editor[e]->setFocus();
        } break;

        case Qt::Key_Comma: /* Toggle modal */
        {
            if (modeModal)
            {
                QPalette p;
                p.setColor(QPalette::Base,
                           QColor(30,30,30));
                notepadqt->setPalette(p);
                modeModal = false;
            }
            else
            {
                QPalette p;
                p.setColor(QPalette::Base,
                           QColor(50,50,50));
                notepadqt->setPalette(p);
                modeModal = true;
            }
        } break;

        case Qt::Key_F:     /* Search */
        {
            search->toggleSearch();
        } break;

        case Qt::Key_O:     /* Toggle file-explorer */
        {
            explorer->toggleExplorer(activeEditor, activeEditor->path);
        } break;

        default:
            return false;
    }
    return true;
}


bool EventEater::shiftModifier(QKeyEvent *keyEvent)
{
    activeEditor->inputBuffer.append(keyEvent->text());

    switch (keyEvent->key())
    {
        case Qt::Key_F3:
        {
            //activeEditor->searchPrev();
        } break;

        default:
            return false;
    }
    return true;
}
