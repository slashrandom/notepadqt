/*
 *	notepadqt.h
 *  This file is part of NotepadQt
 *
 *  Copyright (C) 2019, Stian Halstensen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NOTEPADQT_H
#define NOTEPADQT_H

#include <QMainWindow>

class Editor;
class EventEater;
class FileExplorer;
class QGridLayout;
class QLabel;
class Search;
class Syntaxhl;

class NotepadQt : public QMainWindow
{
    Q_OBJECT

public:
    NotepadQt(QWidget *parent = nullptr);
    ~NotepadQt() override;
    void cmdLineFileToOpen(const QString &s);
    void updateStatus() { curPosChanged(); }

    Editor *activeEditor { nullptr };

private:
    enum class Action
    {
        COPY,
        PASTE,
        CUT,
        UNDO,
        REDO,
        DELETELINE,
        DUPLICATE,
        SWAPDEFIMP,
        CLOSE,
        EXIT,
        FIND,
        FONT,
        NEW,
        OPEN,
        PRINT,
        SAVE,
        SAVEAS,
        DECREASE,
        INCREASE,
        BUILD,
        ABOUT
    };

    QWidget      *notepadqt     { nullptr };
    QLabel       *status[2]     { nullptr, nullptr };
    Editor       *editor[2]     { nullptr, nullptr };
    Search       *search        { nullptr };
    EventEater   *eventeater    { nullptr };
    FileExplorer *fileExplorer  { nullptr };

    inline void initLayout();
    inline void initMenubar();
    inline void initTheme();
    inline void showInfo();
    inline void signalHandler(Action);
    int editorLostFocus;

private slots:
    void curPosChanged();
    void textChanged();
};

#endif // NOTEPADQT_H
