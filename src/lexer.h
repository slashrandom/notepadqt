/*
 *	lexer.h
 *  This file is part of NotepadQt
 *
 *  Copyright (C) 2019, Stian Halstensen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LEXER_H
#define LEXER_H

#include <QStringList>
#include <chrono>

class Lexer
{

public:
    Lexer(const QString &s = "");
    ~Lexer();

    struct Lexdata
    {
        quint64     iterations = 0;
        quint64     keywords = 0;
        QStringList enums;
        QStringList func;
        QStringList includes;
        QStringList var;
    };
    Lexdata data;

    void lexBuffer(const QString &s);           /* Run-time check */
    QString getSuggestion(QString searchword);

private:
    bool check_keyword(const QString&, QStringList*);
    void lexfile();
    void print_result();
    void read_keyfile();

    const short NUM_KEYW = 75;
    QStringList *keywords { nullptr };
    QString     editorfile;

    QString     lastSearchWord;
    QStringList filteredSearchResult;
    int         numCurrentSearchWord;

    QString     lastLexedKeyword;               /* Run-time check */
    bool        lexedKeyword;                   /* Run-time check */

    std::chrono::_V2::system_clock::time_point timer_begin;
};

#endif // LEXER_H
