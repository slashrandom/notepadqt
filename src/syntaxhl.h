/*
 *	syntaxhl.h
 *  This file is part of NotepadQt
 *
 *  Copyright (C) 2019, Stian Halstensen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SYNTAXHL_H
#define SYNTAXHL_H

#include <QSyntaxHighlighter>
#include <QRegularExpression>

class QTextCharFormat;

class Syntaxhl : public QSyntaxHighlighter
{
    Q_OBJECT

public:
    Syntaxhl(QTextDocument *parent = nullptr, int = 10);

protected:
    void highlightBlock(const QString &text) override;

private:
    struct HighlightingRule
    {
        QRegularExpression  pattern;
        QTextCharFormat     format;
    };

    QVector<HighlightingRule>   highlightingRules;
    QRegularExpression          commentStartExpression;
    QRegularExpression          commentEndExpression;
    QTextCharFormat             keywordFormat;
    QTextCharFormat             classFormat;
    QTextCharFormat             singleLineCommentFormat;
    QTextCharFormat             multiLineCommentFormat;
    QTextCharFormat             quotationFormat;
    QTextCharFormat             functionFormat;
    QTextCharFormat             varFormat;
};

#endif // SYNTAXHL_H
