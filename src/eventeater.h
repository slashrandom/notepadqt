/*
 *	eventeater.h
 *  This file is part of NotepadQt
 *
 *  Copyright (C) 2019, Stian Halstensen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef EVENTEATER_H
#define EVENTEATER_H

#include <QObject>

class Editor;
class FileExplorer;
class NotepadQt;
class QKeyEvent;
class QLabel;
class Search;

class EventEater : public QObject
{
    Q_OBJECT

public:
    EventEater(NotepadQt *nqt = nullptr);
    /* References to active objects */
    Editor       *editor[2] { nullptr, nullptr };
    QLabel       *status[2] { nullptr, nullptr };
    FileExplorer *explorer  { nullptr };
    Search       *search    { nullptr };
    Editor       *dragdrop  { nullptr };

protected:
    bool eventFilter(QObject*, QEvent*) override;

private:
    enum ModalKeys
    {
        Modal_Commandlister   = Qt::Key_H,
        Modal_Commentinsert   = Qt::Key_Y,
        Modal_Commentremove   = Qt::Key_Y,
        Modal_Copy            = Qt::Key_C,
        Modal_Copyline        = Qt::Key_F,
        Modal_Cursor_down     = Qt::Key_S,
        Modal_Cursor_left     = Qt::Key_A,
        Modal_Cursor_right    = Qt::Key_D,
        Modal_Cursor_up       = Qt::Key_W,
        Modal_Cut             = Qt::Key_X,
        Modal_Cutline         = Qt::Key_Z,
        Modal_Exit            = Qt::Key_0,
        Modal_Goendofline     = Qt::Key_E,
        Modal_Gostartofline   = Qt::Key_Q,
        Modal_Inserttodo      = Qt::Key_T,
        Modal_Killbuffer      = Qt::Key_K,
        Modal_Killrect        = Qt::Key_R,
        Modal_Listbuffers     = Qt::Key_B,
        Modal_Moveendofbuffer = Qt::Key_End,
        Modal_Movestartofbuff = Qt::Key_Home,
        Modal_Open_file       = Qt::Key_O,
        Modal_Paste           = Qt::Key_V,
        Modal_Setmark         = Qt::Key_Space,
        Modal_Sort            = Qt::Key_M,
        Modal_Text_tolower    = Qt::Key_L,
        Modal_Text_toupper    = Qt::Key_U
    };

    bool modalHandler(QKeyEvent*);
    bool noModifier(QKeyEvent*);
    bool altModifier(QKeyEvent*);
    bool ctrlModifier(QKeyEvent*);
    bool shiftModifier(QKeyEvent*);
    bool modeModal;

    Editor *activeEditor { nullptr };
    NotepadQt *notepadqt { nullptr };
};

#endif // EVENTEATER_H

