/*
 *	search.cpp
 *  This file is part of NotepadQt
 *
 *  Copyright (C) 2019, Stian Halstensen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "search.h"

#include <QAction>
#include <QDebug>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QTextCursor>
#include <QToolButton>

#include "editor.h"
#include "notepadqt.h"


Search::Search(QWidget *parent, NotepadQt *nqt) :
    QWidget(parent),
    notepadqt{ nqt }
{
    if (!notepadqt)
        return;

    /* Define a grid-layout */
    QGridLayout *grid = new QGridLayout;

    /* Add text-labels */
    QLabel *label1 = new QLabel;
    QLabel *label2 = new QLabel;

    /* Add two enter-text fields */
    searchText  = new QLineEdit;
    replaceText = new QLineEdit;

    /* Add ok and cancel button */
    QToolButton *bFind       = new QToolButton(this);
    QToolButton *bFindPrev   = new QToolButton(this);
    QToolButton *bReplace    = new QToolButton(this);
    QToolButton *bReplaceAll = new QToolButton(this);
    QToolButton *bCancel     = new QToolButton(this);


    /* Assign key-actions for tools
     * ---
     * Enter default search?
     * Esc -> cancel
     *
     */

    // TODO: Checkbox for wholeword and ccase sensitivity

    /* Define default button action */
    QAction *aFind = new QAction("", bFind);
    connect(aFind, &QAction::triggered, this, &Search::find);

    QAction *aFindPrev = new QAction("", bFindPrev);
    connect(aFindPrev, &QAction::triggered, this, &Search::findPrev);

    QAction *aReplace = new QAction("", bReplace);
    connect(aReplace, &QAction::triggered, this, &Search::replace);

    QAction *aReplaceAll = new QAction("", bReplaceAll);
    connect(aReplaceAll, &QAction::triggered, this, &Search::replaceAll);

    QAction *aCancel = new QAction("", bCancel);
    connect(aCancel, &QAction::triggered, this, &Search::cancel);

    /* Assign action to buttons */
    bFind->setDefaultAction(aFind);
    bFindPrev->setDefaultAction(aFindPrev);
    bReplace->setDefaultAction(aReplace);
    bReplaceAll->setDefaultAction(aReplaceAll);
    bCancel->setDefaultAction(aCancel);

    /* Set fixed size labels */
    label1->setFixedWidth(90);
    label2->setFixedWidth(90);

    /* Set fixed input fields */
    searchText->setFixedWidth(300);
    replaceText->setFixedWidth(300);

    /* Set fixed button-size */
    bFind->setFixedSize(110,32);
    bFindPrev->setFixedSize(110,32);
    bReplace->setFixedSize(110,32);
    bReplaceAll->setFixedSize(110,32);
    bCancel->setFixedSize(110,32);

    /* Define and setup grid-layout */
    grid->setSpacing(5);
    grid->setVerticalSpacing(5);
    grid->addWidget(label1,0,0,1,1);
    grid->addWidget(searchText,0,1,1,1);
    grid->addWidget(label2,1,0,1,1);
    grid->addWidget(replaceText,1,1,1,1);
    grid->addWidget(bFind,0,2,1,1);
    grid->addWidget(bFindPrev,0,3,1,1);
    grid->addWidget(bReplace,1,2,1,1);
    grid->addWidget(bReplaceAll,1,3,1,1);
    grid->addWidget(bCancel,1,4,1,1);
    this->setLayout(grid);

    /* Define widget size */
    this->setFixedHeight(90);
    this->setFixedWidth(800);

    /* Assign text to widgets */
    label1->setText("Find:");
    label2->setText("Replace with:");
    bFind->setText("Find");
    bFindPrev->setText("Find Prev");
    bReplace->setText("Replace");
    bReplaceAll->setText("Replace All");
    bCancel->setText("Cancel");

    /* Start hidden */
    this->hide();
}

void Search::toggleSearch()
{
    if (!notepadqt)
        return;

    this->isVisible() ? this->hide() : this->show();

    if (this->isVisible())
        searchText->setFocus();
}

void Search::find()
{
    // Add check in searchword, everytime word changes,
    // toggle cursor movePosition(QTextCursor::Start)

    /* Search */
    notepadqt->activeEditor->find(searchText->text());
}

void Search::findPrev()
{
    notepadqt->activeEditor->find(searchText->text(), QTextDocument::FindBackward);
}

void Search::replace()
{
    if (replaceText->text() == "")
        return;

    notepadqt->activeEditor->insertPlainText(replaceText->text());
}

void Search::replaceAll()
{
    /* Move cursor to start of document before starting search */
    QTextCursor cursor = notepadqt->activeEditor->textCursor();
    cursor.movePosition(QTextCursor::Start, QTextCursor::MoveAnchor);
    notepadqt->activeEditor->setTextCursor(cursor);

    /* Replace all occurrences */
    while (notepadqt->activeEditor->find(searchText->text()))
    {
        notepadqt->activeEditor->insertPlainText(replaceText->text());
    }
}

void Search::cancel()
{
    this->hide();
}
