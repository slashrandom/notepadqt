/*
 *	syntaxhl.cpp
 *  This file is part of NotepadQt
 *
 *  Copyright (C) 2019, Stian Halstensen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "syntaxhl.h"

#include <QTextDocument>    // Holds formatted text (to present highlighting).
#include <QTextStream>      // Interface for reading and writing text


#define SYNTAX_CPP ":/keywords/syntax_cpp.txt" // Temp...
#define SYNTAX_SQF ":/keywords/syntax_sqf.txt" // Temp...

#define RGB_BLUE        42,179,252,200
#define RGB_BROWN       206,145,120,220
#define RGB_CYAN        78,201,176,220
#define RGB_DARKGREY    121,122,122,255
#define RGB_GREY        212,212,212,200
#define RGB_GREEN       106,153,76,200
#define RGB_LIGHTBLUE   154,207,214,214
#define RGB_LIGHTCYAN   0,255,255,220
#define RGB_LIGHTGREY   200,200,200,15
#define RGB_LIGHTGREY2  229,229,229,210
#define RGB_LIGHTPINK   214,154,167,214
#define RGB_LIGHTYELLOW 220,220,170,220
#define RGB_PINK        255,128,128,255
#define RGB_PURPLE      197,134,192,220
#define RGB_RED         209,105,105,220
#define RGB_YELLOW      255,255,0,255


// TODO: Optimize, this is WAY WAY WAY to slow.

Syntaxhl::Syntaxhl(QTextDocument *parent, int syntax) :
    QSyntaxHighlighter(parent)
{
    HighlightingRule rule;
    keywordFormat.setForeground(QColor(RGB_BLUE));

    QString filesyntax{""};

    switch (syntax)
    {
        case 0:
        {
            filesyntax = SYNTAX_CPP;
        } break;

        case 1:
        {
            filesyntax = SYNTAX_SQF;
        } break;

        default:
        {
            filesyntax = "";
        } break;
    }

    if (filesyntax == "")
        return;

    QFile file(filesyntax);
    if (!file.open(QIODevice::ReadOnly | QFile::Text))
        return;

    QStringList keywordPatterns;
    QTextStream in(&file);

    // Read file...
    // TODO: Handle prefix \b \i and display accordingly.
    // Could colors be handled this way as well?
    // e.g. \b\colorhex\keyword
    while (!file.atEnd())
    {
        QString line { file.readLine().simplified() };
        line.insert(0, QString("\\b"));
        line.append("\\b");
        keywordPatterns << line;
    }
    file.close();

    // Setup rules and tables...
    foreach (const QString &pattern, keywordPatterns)
    {
        rule.pattern = QRegularExpression(pattern);
        rule.format = keywordFormat;
        highlightingRules.append(rule);
    }

    /* Classnames */
    classFormat.setForeground(QColor(RGB_RED));
    rule.pattern = QRegularExpression("\\b[A-Za-z]+::\\b");
    rule.format = classFormat;
    highlightingRules.append(rule);

    /* General functions */
    functionFormat.setForeground(QColor(RGB_LIGHTYELLOW));
    rule.pattern = QRegularExpression("\\b[A-Za-z0-9_]+(?=\\()");
    rule.format = functionFormat;
    highlightingRules.append(rule);

    /* # */
    varFormat.setForeground(QColor(RGB_PURPLE));
    rule.pattern = QRegularExpression("#[A-Za-z]*");
    rule.format = varFormat;
    highlightingRules.append(rule);

    /* Underscore names */
    // TODO: Multiple underscores
    varFormat.setForeground(QColor(RGB_LIGHTGREY2));
    rule.pattern = QRegularExpression("_[A-Za-z]*");
    rule.format = varFormat;
    highlightingRules.append(rule);

    /* Strings */
    quotationFormat.setForeground(QColor(RGB_BROWN));
    rule.pattern = QRegularExpression("\".*\"");
    rule.format = quotationFormat;
    highlightingRules.append(rule);

    /* < > */
    quotationFormat.setForeground(QColor(RGB_BROWN));
    rule.pattern = QRegularExpression("<.*>");
    rule.format = quotationFormat;
    highlightingRules.append(rule);

    /* Comments */
    // TODO: /// Comments
    singleLineCommentFormat.setForeground(QColor(RGB_GREEN));
    rule.pattern = QRegularExpression("//[^\n]*");
    rule.format = singleLineCommentFormat;
    highlightingRules.append(rule);

    multiLineCommentFormat.setForeground(QColor(RGB_GREEN));
    commentStartExpression = QRegularExpression("/\\*");
    commentEndExpression = QRegularExpression("\\*/");
}

void Syntaxhl::highlightBlock(const QString &text)
{
    foreach (const HighlightingRule &rule, highlightingRules)
    {
        QRegularExpressionMatchIterator matchIterator
        {
            rule.pattern.globalMatch(text)
        };

        while (matchIterator.hasNext())
        {
            QRegularExpressionMatch match{ matchIterator.next() };
            setFormat(match.capturedStart(),
                      match.capturedLength(),
                      rule.format);
        }
    }
    setCurrentBlockState(0);

    int startIndex{ 0 };
    if (previousBlockState() != 1)
        startIndex = text.indexOf(commentStartExpression);

    while (startIndex >= 0)
    {
        QRegularExpressionMatch match
        {
            commentEndExpression.match(text, startIndex)
        };

        int endIndex{ match.capturedStart() };
        int commentLength{ 0 };

        if (endIndex == -1)
        {
            setCurrentBlockState(1);
            commentLength = text.length() - startIndex;
        }
        else
        {
            commentLength = endIndex
                    - startIndex
                    + match.capturedLength();
        }

        setFormat(startIndex, commentLength, multiLineCommentFormat);
        startIndex = text.indexOf(
                    commentStartExpression,
                    startIndex + commentLength);
    }
}
