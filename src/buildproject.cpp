/*
 *	buildproject.cpp
 *  This file is part of NotepadQt
 *
 *  Copyright (C) 2019, Stian Halstensen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "buildproject.h"

#include <QFileInfo>
#include <QWidget>
#include <QVBoxLayout>
#include <QPlainTextEdit>
#include <QProcess>


BuildProject::BuildProject(QString path)
{
    if (path == "")
        return;

    QString command {""};

#ifdef Q_OS_WIN
    command.append("/build.bat");
#endif

#ifdef Q_OS_LINUX
    command.append("/build");
#endif

#ifdef Q_OS_MACOS
    command.append("/build");
#endif
    /*
     * Check if file exists in current dir, otherwise keep moving
     * up until a makefile is found - MAX 3 levels.
     * How to define a project root?
     */

    bool exists{false};
    for (int i{0}; i<3; ++i)
    {
        //qDebug() << path+command;
        QFileInfo file(path+command);
        if (file.exists() && file.isFile())
        {
            exists = true;
            break;
        }

        while (!path.endsWith("/"))
        {
            path.remove(path.length()-1,1);
        }
        path.remove(path.length()-1,1);
    }

    if (!exists)
        return;

    // Create Build-window and capture output
    QWidget *w = new QWidget;
    QVBoxLayout *vbl = new QVBoxLayout;
    QPlainTextEdit *pte = new QPlainTextEdit;
    vbl->addWidget(pte);
    w->setLayout(vbl);
    w->show();
    w->setFixedSize(widgetWidth,widhetHeight);
    w->setWindowTitle("Build status:");

    QProcess *process = new QProcess;
    connect(process, &QProcess::readyReadStandardOutput, [=](){
        pte->appendPlainText(process->readAllStandardOutput());
    });
    connect(process, &QProcess::readyReadStandardError, [=](){
        pte->appendPlainText(process->readAllStandardError());
    });

#ifdef Q_OS_WIN
    process->start(path+command);
#endif

#ifdef Q_OS_LINUX
    // TODO: Fix dir jumping
    process->start("sh", QStringList() << "-c"
                   << QString("cd " + path + "&& ./build"));
#endif
}
