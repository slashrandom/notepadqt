/*
 *	notepadqt.cpp
 *  This file is part of NotepadQt
 *
 *  Copyright (C) 2019, Stian Halstensen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "notepadqt.h"

#include <QApplication>     // GUI control flow and main settings
#include <QDesktopWidget>   // Set app-size
#include <QFontDialog>      // Used for selecting fonts
#include <QGridLayout>      // Widget layout
#include <QLabel>           // Status-fields
#include <QMenuBar>         // Menubar
#include <QMessageBox>      // Pop-up messages
#include <QSplitter>        // Re-sizable splitter widget
#include <QStyleFactory>    // Used for theming
#include <QDebug>

#include "buildproject.h"
#include "editor.h"
#include "eventeater.h"
#include "fileexplorer.h"
#include "search.h"


NotepadQt::NotepadQt(QWidget *parent) :
    QMainWindow(parent),
    editorLostFocus{ 0 }
{
    editor[0]    = new Editor;
    editor[1]    = new Editor;
    status[0]    = new QLabel;
    status[1]    = new QLabel;
    eventeater   = new EventEater(this);
    fileExplorer = new FileExplorer;
    search       = new Search(this, this);

    /* TODO:
     * Create statusbars here, passed as *ref to editor on New
     * Move textchanged and cursorpos to editor
     * editor update status
     */

    initTheme();
    initLayout();
    initMenubar();
    showInfo();

    /* Bind signal to slot */
    for (size_t i{0}; i<2; ++i) {
        connect(editor[i], SIGNAL(textChanged()),
                this, SLOT(textChanged()));
        connect(editor[i], SIGNAL(cursorPositionChanged()),
                this, SLOT(curPosChanged()));
    }

    /* Setup EventEater overriding defined keybinds */
    this->installEventFilter(eventeater);

    /* Define object names so they can be
       referenced using *obj->objectName(). */
    editor[0]->setObjectName("Edit1");
    editor[1]->setObjectName("Edit2");

    status[0]->setObjectName("Status1");    // move to editor?
    status[1]->setObjectName("Status2");    // part of the editor instance?

    /* Pass references to the different objects */
    editor[0]->installEventFilter(eventeater);
    eventeater->editor[0] = editor[0];
    eventeater->status[0] = status[0];

    editor[1]->installEventFilter(eventeater);
    eventeater->editor[1] = editor[1];
    eventeater->status[1] = status[1];

    eventeater->explorer  = fileExplorer;
    eventeater->search    = search;
}


NotepadQt::~NotepadQt()
{
    // Auto-save files?
}


void NotepadQt::cmdLineFileToOpen(const QString &s)
{
    // TODO: Handle MS_WIN, file fails to open
    editor[0]->fileOpen(s);
}


void NotepadQt::initLayout()
{
    notepadqt = new QWidget();
    QSplitter *qsplit = new QSplitter;
    QGridLayout *grid = new QGridLayout;

    status[0]->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    status[1]->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

    this->setObjectName("NotepadQt");

    grid->addWidget(status[0],0,0,1,1);
    grid->addWidget(status[1],0,1,1,1);
    grid->addWidget(qsplit,1,0,1,2);
    grid->setSpacing(1);
    grid->addWidget(fileExplorer,0,0,1,2);
    grid->addWidget(search,2,0,1,2);
    qsplit->addWidget(editor[0]);
    qsplit->addWidget(editor[1]);
    qsplit->setHandleWidth(0);
    notepadqt->setLayout(grid);
    setCentralWidget(notepadqt);

    /* Resize app to 80% of total screensize. */
    resize(QDesktopWidget().availableGeometry(this).size() * 0.8);
}


void NotepadQt::initTheme()
{
    // TODO: Change into theming system
    qApp->setStyle(QStyleFactory::create("Fusion"));
    QPalette p;
    p.setColor(QPalette::Window,            // General bg color
               QColor(30,30,30));
    p.setColor(QPalette::WindowText,        // General fg color
               Qt::white);
    p.setColor(QPalette::Disabled,          // Fg color when disabled
               QPalette::WindowText,
               QColor(127,127,127));
    p.setColor(QPalette::Base,              // Bg color for text entry widgets
               QColor(30,30,30));
    p.setColor(QPalette::AlternateBase,     // Alt bg color in views with
               QColor(66,66,66));           // alternating row colors
    p.setColor(QPalette::ToolTipBase,       // Bg color for QToolTip and QWhatsThis
               QColor(30,30,30));
    p.setColor(QPalette::ToolTipText,       // Fg color tooltip
               Qt::white);
    p.setColor(QPalette::Text,              // Fg color used with Base
               QColor(212,212,212,200));
    p.setColor(QPalette::Disabled,          // Fg color used with Base
               QPalette::Text,              // when disabled
               QColor(127,127,127));
    p.setColor(QPalette::Dark,              // Darker than Button.
               QColor(35,35,35));
    p.setColor(QPalette::Shadow,            //
               QColor(20,20,20));
    p.setColor(QPalette::Button,            // General button bg color
               QColor(53,53,53));
    p.setColor(QPalette::ButtonText,        // General button fg color
               Qt::white);
    p.setColor(QPalette::Disabled,          // General button fg color disabled
               QPalette::ButtonText,
               QColor(127,127,127));
    p.setColor(QPalette::BrightText,        // Contrast text
               Qt::red);
    p.setColor(QPalette::Link,              // Color used for unvisited hyperlinks
               QColor(42,130,218));
    p.setColor(QPalette::Highlight,         // Color to indicate a selected item
               QColor(122,123,124));
    p.setColor(QPalette::Disabled,          //
               QPalette::Highlight,
               QColor(80,80,80));
    p.setColor(QPalette::HighlightedText,   //
               Qt::white);
    p.setColor(QPalette::Disabled,          //
               QPalette::HighlightedText,
               QColor(127,127,127));
    p.setColor(QPalette::NoRole,            //
               QColor(0,0,0));
    qApp->setPalette(p);

    // Colorsettings for statusfields...
    for (size_t i{0}; i<2; ++i) {
        status[i]->setStyleSheet("color: black;"
                                 "background-color: rgb(121,122,122);");
    }

    // Default font setup:
    QFontDatabase::addApplicationFont(":/fonts/liberation_mono.ttf");

    QFont font;
    font.setFamily("Liberation Mono");
    //font.setStyleHint(QFont::Monospace);
    font.setFixedPitch(true);
    font.setPointSize(12);
    //qApp->setFont(font);

    status[0]->setFont(font);
    status[1]->setFont(font);
    editor[0]->setFont(font);
    editor[1]->setFont(font);
}


void NotepadQt::initMenubar()
{
    /* --------------------------------------------------------------
        Menubar: File
        https://specifications.freedesktop.org/icon-naming-spec/icon-naming-spec-latest.html
    -------------------------------------------------------------- */
    QMenu *fileMenu = menuBar()->addMenu(tr("File"));
    fileMenu->setObjectName("FileMenu");
    // File -> New
    QAction *newAct = new QAction(tr("New"), this);
    newAct->setShortcuts(QKeySequence::New);
    connect(newAct, &QAction::triggered, this,
            [this]{ signalHandler(Action::NEW); });
    fileMenu->addAction(newAct);


    // File -> Open
    QAction *openAct = new QAction(tr("Open\tCtrl+O"), this);
    //openAct->setShortcuts(QKeySequence::Open);
    connect(openAct, &QAction::triggered, this,
            [this]{ signalHandler(Action::OPEN); });
    fileMenu->addAction(openAct);


    // File -> Save
    QAction *saveAct = new QAction(tr("Save"), this);
    saveAct->setShortcuts(QKeySequence::Save);
    connect(saveAct, &QAction::triggered, this,
            [this]{ signalHandler(Action::SAVE); });
    fileMenu->addAction(saveAct);


    // File -> Save as
    QAction *saveAsAct = new QAction(tr("Save As"), this);
    saveAsAct->setShortcuts(QKeySequence::SaveAs);
    connect(saveAsAct, &QAction::triggered, this,
            [this]{ signalHandler(Action::SAVEAS); });
    fileMenu->addAction(saveAsAct);

    //fileMenu->addSeparator();

    // File -> Print
    QAction *print = new QAction(tr("Print"), this);
    print->setShortcuts(QKeySequence::Print);
    connect(print, &QAction::triggered, this,
            [this]{ signalHandler(Action::PRINT); });
    fileMenu->addAction(print);

    // File -> Close
    QAction *close = new QAction(tr("Close"), this);
    close->setShortcut(QKeySequence(Qt::CTRL | Qt::Key_W));
    connect(close, &QAction::triggered, this,
            [this]{ signalHandler(Action::CLOSE); });
    fileMenu->addAction(close);

    // File -> Exit
    QAction *exitAct = new QAction(tr("Exit"), this);
    //exitAct->setShortcuts(QKeySequence::Quit);
    connect(exitAct, &QAction::triggered, this,
            [this]{ signalHandler(Action::EXIT); });
    fileMenu->addAction(exitAct);


    /* --------------------------------------------------------------
        Menubar: Edit
    -------------------------------------------------------------- */
    QMenu *editMenu = menuBar()->addMenu(tr("Edit"));

    // Edit -> Undo
    QAction *undo = new QAction(tr("Undo"), this);
    undo->setShortcuts(QKeySequence::Undo);
    connect(undo, &QAction::triggered, this,
            [this]{ signalHandler(Action::UNDO); });
    editMenu->addAction(undo);

    // Edit -> Cut
    QAction *cutAct = new QAction(tr("Cut"), this);
    cutAct->setShortcuts(QKeySequence::Cut);
    connect(cutAct, &QAction::triggered, this,
            [this]{ signalHandler(Action::CUT); });
    editMenu->addAction(cutAct);

    // Edit -> Cut
    QAction *copyAct = new QAction(tr("Copy"), this);
    copyAct->setShortcuts(QKeySequence::Copy);
    connect(copyAct, &QAction::triggered, this,
            [this]{ signalHandler(Action::COPY); });
    editMenu->addAction(copyAct);

    // Edit -> Paste
    QAction *pasteAct = new QAction(tr("Paste"), this);
    pasteAct->setShortcuts(QKeySequence::Paste);
    connect(pasteAct, &QAction::triggered, this,
            [this]{ signalHandler(Action::PASTE); });
    editMenu->addAction(pasteAct);

    //menuBar()->addSeparator();

    // Edit -> Delete line
    QAction *del = new QAction(tr("Delete line"), this);
    del->setShortcut(QKeySequence(Qt::CTRL | Qt::Key_L));
    connect(del, &QAction::triggered, this,
            [this]{ signalHandler(Action::DELETELINE); });
    editMenu->addAction(del);

    // Edit -> Duplicate line
    QAction *dup = new QAction(tr("Duplicate line"), this);
    dup->setShortcut(QKeySequence(Qt::CTRL | Qt::Key_D));
    connect(dup, &QAction::triggered, this,
            [this]{ signalHandler(Action::DUPLICATE); });
    editMenu->addAction(dup);


    /* --------------------------------------------------------------
        Menubar: Find
    -------------------------------------------------------------- */
    QMenu *findMenu = menuBar()->addMenu(tr("Find"));

    // Find -> Find
    QAction *find = new QAction(tr("Find\tCtrl+F"), this);
    connect(find, &QAction::triggered, this,
            [this]{ signalHandler(Action::FIND); });
    findMenu->addAction(find);

    /* --------------------------------------------------------------
        Menubar: View
    -------------------------------------------------------------- */
    QMenu *viewMenu = menuBar()->addMenu(tr("View"));

    // View -> Increase text size
    QAction *increase = new QAction(tr("Increase text size"), this);
    increase->setShortcut(Qt::CTRL | Qt::Key_Plus);
    connect(increase, &QAction::triggered, this,
            [this]{ signalHandler(Action::INCREASE); });
    viewMenu->addAction(increase);

    // View -> Decrease text size
    QAction *decrease = new QAction(tr("Decrease text size"), this);
    decrease->setShortcut(Qt::CTRL | Qt::Key_Minus);
    connect(decrease, &QAction::triggered, this,
            [this]{ signalHandler(Action::DECREASE); });
    viewMenu->addAction(decrease);

    // View -> Select font
    QAction *select = new QAction(tr("Select font"), this);
    connect(select, &QAction::triggered, this,
            [this]{ signalHandler(Action::FONT); });
    viewMenu->addAction(select);


    /* --------------------------------------------------------------
        Menubar: Tools
    -------------------------------------------------------------- */
    QMenu *toolsMenu = menuBar()->addMenu(tr("Tools"));

    // Tools -> Increase text size
    QAction *build = new QAction(tr("Build"), this);
    build->setShortcut(Qt::CTRL | Qt::Key_R);
    connect(build, &QAction::triggered, this,
            [this]{ signalHandler(Action::BUILD); });
    toolsMenu->addAction(build);


    /* --------------------------------------------------------------
        Menubar: Help
    -------------------------------------------------------------- */
    QMenu *helpMenu = menuBar()->addMenu(tr("Help"));

    // Help -> Info
    QAction *infoAct = new QAction(tr("Info"), this);
    connect(infoAct, &QAction::triggered, this,
            [this]{ showInfo(); });
    helpMenu->addAction(infoAct);

    // Help -> About
    QAction *aboutAct = new QAction(tr("About"), this);
    connect(aboutAct, &QAction::triggered, this,
            [this]{ signalHandler(Action::ABOUT); });
    helpMenu->addAction(aboutAct);

    // Help -> About QT
    QAction *aboutQtAct = helpMenu->addAction(tr("About Qt"),
                                              qApp, &QApplication::aboutQt);
    helpMenu->addAction(aboutQtAct);
}


void NotepadQt::showInfo()
{
    // Show keybinds...
    QFile file(":/docs/info.txt");
    if (file.open(QIODevice::ReadOnly | QFile::Text))
    {
        QTextStream in(&file);
        QString text = in.readAll();
        editor[1]->fileNew();
        editor[1]->setPlainText(text);
    }
    file.close();
}


void NotepadQt::signalHandler(Action a)
{
    switch (a)
    {
        case Action::COPY:
        {
            activeEditor->copy();  /* Qt built-in feature. */
        } break;

        case Action::PASTE:
        {
            activeEditor->paste(); /* Qt built-in feature. */
        } break;

        case Action::CUT:
        {
            activeEditor->cut();   /* Qt built-in feature. */
        } break;

        case Action::UNDO:
        {
            activeEditor->undo();  /* Qt built-in feature. */
        } break;

        case Action::REDO:
        {
            activeEditor->redo();  /* Qt built-in feature. */
        } break;

        case Action::DELETELINE:
        {
            activeEditor->lineDelete();
        } break;

        case Action::DUPLICATE:
        {
            activeEditor->lineDuplicate();
        } break;

        case Action::SWAPDEFIMP:
        {
            // TODO: Add to menu
        } break;

        case Action::CLOSE:
        {
            signalHandler(Action::NEW);
        } break;

        case Action::EXIT:
        {
            // TODO: Throw a "really quit?" warning...
            // TODO: Check if save is req
            QApplication::quit();
        } break;

        case Action::FIND:
        {
            search->toggleSearch();
        } break;

        case Action::FONT:
        {
            bool fontSelected;
            QFont font = QFontDialog::getFont(&fontSelected, this);
            if (fontSelected) {
                editor[0]->setFont(font);
                editor[1]->setFont(font);
            }
        } break;

        case Action::NEW:
        {
            activeEditor->fileNew();
            curPosChanged();
        } break;

        case Action::OPEN:
        {
            activeEditor->fileOpen();
        } break;

        case Action::PRINT:
        {
            activeEditor->filePrint();
        } break;

        case Action::SAVE:
        {
            activeEditor->fileSave();
            curPosChanged();
        } break;

        case Action::SAVEAS:
        {
            activeEditor->fileSaveAs();
            curPosChanged();
        } break;

        case Action::DECREASE:
        {
            activeEditor->zoomOut(1);
        } break;

        case Action::INCREASE:
        {
            activeEditor->zoomIn(1);
        } break;

        case Action::BUILD:
        {
            BuildProject(activeEditor->path);
        } break;

        case Action::ABOUT:
        {
            QMessageBox mb;
            //mb.setIconPixmap(QPixmap("coffee-cup-icon.png"));
            mb.setStyleSheet("QLabel{min-width: 350px;}");
            mb.setWindowTitle("About...");
            mb.setText("NotedpadQt\n\n"
                       "Build: " __DATE__ " -- " __TIME__
                       "\n\n"
                       "Copyright 2019 - Stian Halstensen\n\n"
                       "Git: https://gitlab.com/slashrandom/notepadqt/");
            mb.exec();
        } break;
    }
}


void NotepadQt::textChanged()
{
    if (activeEditor->openCalled)
    {
        activeEditor->openCalled = false;
    }
    else
    {
        QString s { activeEditor->toPlainText() };
        if (!s.isEmpty())
            activeEditor->savingRequired = true;
    }
    curPosChanged();
}


void NotepadQt::curPosChanged()
{
    int s{0};   // Statusfield
    if (activeEditor->objectName() == "Edit2")
        s = 1;

    QTextCursor cursor = activeEditor->textCursor();
    int y { cursor.blockNumber() + 1 };
    int x { cursor.columnNumber() + 1 };

    QString saveIndicator{" "};
    if (activeEditor->savingRequired)
        saveIndicator = "*";

    QString filename{""};
    if (activeEditor->filename == "")
        filename = "Untitled";
    else
        filename = activeEditor->filename;

    status[s]->setText(
                QString(" %1 %2 %1 -  L#%3 C#%4")
                .arg(saveIndicator)
                .arg(filename)
                .arg(y)
                .arg(x));
}
