/*
 *	buildproject.h
 *  This file is part of NotepadQt
 *
 *  Copyright (C) 2019, Stian Halstensen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef BUILDPROJECT_H
#define BUILDPROJECT_H

#include <QObject>
#include <QString>

class BuildProject : public QObject
{
    Q_OBJECT

public:
    BuildProject(QString path = "");

private:
    int widgetWidth  { 500 };     /* Size of build-window */
    int widhetHeight { 500 };     /* Size of build-window */
};

#endif // BUILDPROJECT_H
