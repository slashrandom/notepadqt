/*
 *	editor.cpp
 *  This file is part of NotepadQt
 *
 *  Copyright (C) 2019, Stian Halstensen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "editor.h"

#include <QFile>            // Obj for reading and writing files
#include <QFileDialog>      // Default file dialog
#include <QMessageBox>      // Pop-up messages
#include <QPrinter>         // Print support
#include <QPrintDialog>     // User print dialog
#include <QtWidgets>

#include "syntaxhl.h"
#include "lexer.h"


Editor::Editor(QWidget *parent) :
    QPlainTextEdit(parent),
    openCalled      { false },
    savingRequired  { false }
{
    lineNumberArea = new LineNumberArea(this);

    /* Qt signals triggering updates... */
    connect(this, SIGNAL(blockCountChanged(int)),
            this, SLOT(updateLineNumberAreaWidth(int)));
    connect(this, SIGNAL(updateRequest(QRect,int)),
            this, SLOT(updateLineNumberArea(QRect,int)));
    connect(this, SIGNAL(cursorPositionChanged()),
            this, SLOT(highlightCurrentLine()));

    this->setCenterOnScroll(true);
    //this->setAcceptDrops(true); // Accept drops are handled by eventeater

    updateLineNumberAreaWidth(0);
    highlightCurrentLine();
    resetEditor();
}


void Editor::cursorMove(Editor::Direction dir)
{
    QTextCursor cursor = this->textCursor();
    switch (dir)
    {
        case Direction_Up:
        {
            cursor.movePosition(QTextCursor::Up,
                                QTextCursor::MoveAnchor,
                                1);
        } break;

        case Direction_Down:
        {
            cursor.movePosition(QTextCursor::Down,
                                QTextCursor::MoveAnchor,
                                1);
        } break;

        case Direction_Left:
        {
            cursor.movePosition(QTextCursor::Left,
                                QTextCursor::MoveAnchor,
                                1);
        } break;

        case Direction_Right:
        {
            cursor.movePosition(QTextCursor::Right,
                                QTextCursor::MoveAnchor,
                                1);
        } break;

        case Direction_Beginning:
        {
            cursor.movePosition(QTextCursor::Start);
        } break;

        case Direction_End:
        {
            cursor.movePosition(QTextCursor::End);
        } break;
    }
    this->setTextCursor(cursor);
}


void Editor::cursorStep(Editor::Direction dir)
{
    QTextCursor cursor = this->textCursor();
    switch (dir)
    {
        case Direction_Up:
        {
            cursor.movePosition(QTextCursor::Up,
                                QTextCursor::MoveAnchor,
                                10);
        } break;

        case Direction_Down:
        {
            cursor.movePosition(QTextCursor::Down,
                                QTextCursor::MoveAnchor,
                                10);
        } break;

        case Direction_Left:
        {
            cursor.movePosition(QTextCursor::StartOfLine);
        } break;

        case Direction_Right:
        {
            cursor.movePosition(QTextCursor::EndOfLine);
        } break;

        case Direction_Beginning:
        {
            cursor.movePosition(QTextCursor::Start);
        } break;

        case Direction_End:
        {
            cursor.movePosition(QTextCursor::End);
        } break;
    }
    this->setTextCursor(cursor);
}


void Editor::fileCloseWithoutSave()
{
    QMessageBox mb;
    //mb.setIconPixmap(QPixmap("coffee-cup-icon.png"));
    mb.setStyleSheet("QLabel{min-width: 350px;}");
    mb.setWindowTitle("Save");
    mb.setText("Save file before closing?");
    mb.setStandardButtons(QMessageBox::Save
                          | QMessageBox::Discard
                          | QMessageBox::Cancel);
    mb.setDefaultButton(QMessageBox::Save);
    int ret {mb.exec()};

    switch (ret)
    {
        case QMessageBox::Save:
        {
            fileSaveAs();
        } break;

        case QMessageBox::Discard:
        {
            this->savingRequired = false;
            fileNew();
        } break;

        case QMessageBox::Cancel:
        {
            // Abandon...
        } break;

        default:
        {
            // Should never be reached
        } break;
    }
}


void Editor::fileNew()
{
    if (this->savingRequired)
    {
        fileCloseWithoutSave();
    }
    else
    {
        this->clear();
        this->resetEditor();
    }
    delete lexer;
    lexer = nullptr;
}


void Editor::fileOpen(const QString &f)
{
    /* Will check if saving is req and clear */
    // TODO: If user aborts file-open, the editor is empty,
    // prob not pref. Ask for save if req, then open, and if
    // a file opens successfully, then clear....

    QString fileName{""};

    if (f == "")
    {
        fileNew();

        fileName = QFileDialog::getOpenFileName(this, ("Open"));
        if (fileName == "")
            return;
    }
    else
    {
        fileName = f;
    }

    QFile file(fileName);
    /*Throw an error and return if file failed to open */
    if (!file.open(QIODevice::ReadOnly | QFile::Text)) {
        QMessageBox::warning(this, "Warning", "Failed to open file: " +
                             file.errorString());
        return;
    }

    /* Replace \t (tabs) with 4x" " (spaces) */
    QString text;
    while (!file.atEnd())
    {
        QString line { file.readLine() };
        for (int i{0}; i<line.length();++i)
        {
            if (line[i] == '\t')
                text.append("    ");
            else
                text.append(line[i]);
        }
        line.clear();
    }

    // Set current file
    QFileInfo fi{ file };
    this->filepath    = fileName;
    this->filename    = fi.fileName();
    this->suffix      = fi.suffix();
    this->path        = fi.absolutePath();
    this->openCalled  = true;
    this->setPlainText(text);
    file.close();
    this->initSyntaxHl(); // Own thread to prevent delaying opening of file
    // Action::TEXTCHANGED is triggered twice during opening.
    this->openCalled  = true;
    lexer = new Lexer(fi.absoluteFilePath());
}


void Editor::filePrint()
{
    QPrinter printer;
    printer.setPrinterName("Print");
    QPrintDialog pDialog(&printer, this);

    if (pDialog.exec() == QDialog::Rejected) {
        QMessageBox::warning(this, "Warning", "Cannot Access Printer");
        return;
    }

    this->print(&printer);
}


void Editor::fileSave()
{
    if (this->filename == "") {
        fileSaveAs();
        return;
    }

    QFile file(this->filepath);
    if (!file.open(QFile::WriteOnly | QFile::Text)) {
        QMessageBox::warning(this, "Warning", "Cannot save file: " +
                             file.errorString());
        return;
    }

    QTextStream out(&file);
    QString text{ this->toPlainText() };
    out << text;
    file.close();

    this->savingRequired = false;
}


void Editor::fileSaveAs()
{
    QString fileName = QFileDialog::getSaveFileName(this, "Save");
    QFile file(fileName);
    if (fileName == "")
        return;

    if (!file.open(QFile::WriteOnly | QFile::Text)) {
        QMessageBox::warning(this, "Warning", "Cannot save file: " +
                             file.errorString());
        return;
    }

    QFileInfo fi{ file };
    this->filepath    = fileName;
    this->filename    = fi.fileName();
    this->suffix      = fi.suffix();
    this->path        = fi.absolutePath();

    this->savingRequired = false;

    QTextStream out(&file);
    QString text{ this->toPlainText() };
    out << text;
    file.close();

    this->initSyntaxHl();
}


void Editor::toLower()
{
    QTextCursor cursor = this->textCursor();
    if (!cursor.hasSelection()) return;

    QString str = cursor.selectedText();
    QString formatedstr = str.toLower();
    cursor.insertText(formatedstr);
}


void Editor::toUpper()
{
    QTextCursor cursor = this->textCursor();
    if (!cursor.hasSelection()) return;

    QString str = cursor.selectedText();
    QString formatedstr = str.toUpper();
    cursor.insertText(formatedstr);
}


void Editor::toggleBlockComment()
{
    //
}


void Editor::toggleLineComment()
{
    //
}


void Editor::lineDuplicate()
{
    /* Get the textline under cursor */
    QTextCursor cursor = this->textCursor();
    cursor.select(QTextCursor::LineUnderCursor);
    QString selectedText = cursor.selectedText();

    if (selectedText != "")
    {
        cursor.clearSelection();
        cursor.insertText(QString("\n") + selectedText);
    }
}


void Editor::lineDelete()
{
    QTextCursor cursor = this->textCursor();
    cursor.select(QTextCursor::LineUnderCursor);
    cursor.removeSelectedText();
}


int Editor::lineNumberAreaWidth()
{
    int digits = 1;
    int max = qMax(1, blockCount());
    while (max >= 10)
    {
        max /= 10;
        ++digits;
    }

    // Total width of the numberbar
    int space = 3 + fontMetrics().horizontalAdvance(QLatin1Char('9')) * digits;

    return space;
}


void Editor::initSyntaxHl()
{
    int t{};
    QString s = suffix;

    if (s == "cpp")
        t = 0;
    else if (s == "h")
        t = 0;
    else if (s == "sqf")
        t = 1;
    else
        return;

    if (syntaxhl) {
        delete syntaxhl;
        syntaxhl = nullptr;
    }

    syntaxhl = new Syntaxhl(this->document(), t);
}


void Editor::resetEditor()
{
    filepath        = "";
    filename        = "";
    path            = "";
    suffix          = "";
    searchString    = "";
    openCalled      = false;
    savingRequired  = false;

    if (syntaxhl) {
        delete syntaxhl;
        syntaxhl = nullptr;
    }
}


void Editor::insertTemplate(Template t)
{   // TODO: Create a snippet-list??
    switch (t)
    {
        case Template_For:
        {
            this->insertPlainText("for (int i{}; i< ; ++i) \n{\n\n}\n");
        } break;

        case Template_Switch:
        {
            QString s = {""};
            s.append("switch ()\n{\n");
            s.append("    case :\n    {\n\n    } break;\n\n");
            s.append("    case :\n    {\n\n    } break;\n\n");
            s.append("    case :\n    {\n\n    } break;\n\n");
            s.append("    default:\n    {\n\n    } break;\n}\n");
            this->insertPlainText(s);
        } break;
    }
}


void Editor::indent()
{
    QTextCursor cursor = this->textCursor();
    // TODO: Fix this ->
    int x = cursor.columnNumber() + 5;
    int space{4};
    int s{4};
    while (x % space != 0)
    {
        x--;
        s--;
    }

    QString tab{""};
    for (int i{0}; i<=s; ++i)
        tab.append(" ");

    if (cursor.hasSelection())
    {
        QTextBlock blockSelection;
        blockSelection.blockNumber();

        int spos = cursor.anchor();
        int epos = cursor.position();
        if (spos > epos) std::swap(spos, epos);

        cursor.setPosition(spos, QTextCursor::MoveAnchor);
        int sblock = cursor.block().blockNumber();

        cursor.setPosition(epos, QTextCursor::MoveAnchor);
        int eblock = cursor.block().blockNumber();

        cursor.setPosition(spos, QTextCursor::MoveAnchor);
        const int diff = eblock - sblock;

        cursor.beginEditBlock();
        for (int i{0}; i <= diff; ++i)
        {
            cursor.movePosition(QTextCursor::StartOfBlock,
                                QTextCursor::MoveAnchor);
            cursor.insertText(tab);
            cursor.movePosition(QTextCursor::NextBlock,
                                QTextCursor::MoveAnchor);
        }
        cursor.endEditBlock();

        cursor.setPosition(spos, QTextCursor::MoveAnchor);
        cursor.movePosition(QTextCursor::StartOfBlock,
                            QTextCursor::MoveAnchor);

        while (cursor.block().blockNumber() < eblock)
        {
            cursor.movePosition(QTextCursor::NextBlock,
                                QTextCursor::KeepAnchor);
        }

        cursor.movePosition(QTextCursor::EndOfBlock,
                            QTextCursor::KeepAnchor);

        setTextCursor(cursor);
    }
    else
    {
        this->insertPlainText(tab);
    }
}


void Editor::suggestionInsert()
{
    if (!lexer)
        return;

    QTextCursor cursor = this->textCursor();
    cursor.movePosition(QTextCursor::StartOfWord, QTextCursor::KeepAnchor);
    setTextCursor(cursor);
    this->insertPlainText(lexer->getSuggestion(inputBuffer));
    cursor.movePosition(QTextCursor::StartOfWord, QTextCursor::KeepAnchor);
    setTextCursor(cursor);
}


void Editor::suggestionSelect()
{
    QTextCursor cursor = this->textCursor();
    cursor.movePosition(QTextCursor::Right, QTextCursor::MoveAnchor, 1);
    setTextCursor(cursor);
}


void Editor::swapDefImp(const QString &path, const QString &file)
{
    // Check cpp vs h - Get new file extension.
    QFileInfo fi(file);
    QString base { fi.completeBaseName() };
    QString ext  { fi.completeSuffix() };
    QString newFile{};

    // Check if req file exists...
    if (ext == "cpp")
        newFile = (path+"/"+base+".h");
    else
        newFile = (path+"/"+base+".cpp");

    if (QFile::exists(newFile))
        fileOpen(newFile);
}


void Editor::updateLineNumberAreaWidth(int /* newBlockCount */)
{
    setViewportMargins(lineNumberAreaWidth(), 0, 0, 0);
}


void Editor::highlightCurrentLine()
{
    QList<QTextEdit::ExtraSelection> extraSelections;

    if (!isReadOnly()) {
        QTextEdit::ExtraSelection selection;

        QColor lineColor = QColor(211,211,211,35);  // Highlight color 15
        selection.format.setBackground(lineColor);
        selection.format.setProperty(QTextFormat::FullWidthSelection, true);
        selection.cursor = textCursor();
        selection.cursor.clearSelection();
        extraSelections.append(selection);
    }

    setExtraSelections(extraSelections);
}


void Editor::updateLineNumberArea(const QRect &rect, int dy)
{
    if (dy)
        lineNumberArea->scroll(0, dy);
    else
        lineNumberArea->update(0,
                               rect.y(),
                               lineNumberArea->width(),
                               rect.height());

    if (rect.contains(viewport()->rect()))
        updateLineNumberAreaWidth(0);
}


void Editor::getSelection(std::vector<QString> *string_array)
{
    QTextCursor cursor = this->textCursor();
    QTextCursor mover = cursor;

    int sel_start{ cursor.selectionStart() };
    int sel_end{ cursor.selectionEnd() };

    if (sel_start < sel_end)
    {
        sel_end = sel_start;
        mover.setPosition(cursor.selectionEnd());
    }
    else
    {
        mover.setPosition(cursor.selectionStart());
    }

    cursor.setKeepPositionOnInsert(true);
    int line{ this->document()->lineCount() };

    while (line >= 0)
    {
        if (mover.position() == sel_end)
        {
            mover.select(QTextCursor::LineUnderCursor);
            string_array->push_back(mover.selectedText());
            break;
        }
        mover.select(QTextCursor::LineUnderCursor);
        string_array->push_back(mover.selectedText());
        mover.clearSelection();
        mover.movePosition(QTextCursor::Up);
        mover.movePosition(QTextCursor::StartOfLine);
        --line;
    }
}


void Editor::killRectangle()
{
    // TODO: Handle selection -
    // will now only work for beginning of each string

    // Set undo-point before operation

    QTextCursor cursor = this->textCursor();
    if (!cursor.hasSelection())
        return;

    // Calculate elements to remove
    QTextCursor mover = cursor;
    mover.movePosition(QTextCursor::StartOfLine);
    int elements = cursor.position() - mover.position();
    //qDebug() << elements;

    // Get selection
    std::vector<QString> *string_array = new std::vector<QString>;
    getSelection(string_array);

    // Remove X chars from each array element.
    size_t size = string_array->size();
    for (size_t i{0}; i < size; ++i)
    {
        for (int j{elements}; j>0; --j)
        {
            string_array->at(i).remove(0,1);
        }
    }

    for (size_t i{size}; i>0; --i)
    {
        this->insertPlainText(string_array->at(i-1));
        this->insertPlainText("\n");
    }

    //for (auto x: *string_array)
    //    qDebug() << x << " ";

    delete string_array;
    string_array = nullptr;

    // TODO: Reselect after operation.
}


void Editor::removeWhitespace()
{
    QTextCursor cursor = this->textCursor();
    if (!cursor.hasSelection())
        return;

    QTextCursor mover = cursor;
    mover.movePosition(QTextCursor::StartOfLine);

    // Get selection
    std::vector<QString> *string_array = new std::vector<QString>;
    getSelection(string_array);

    // Remove " " from each array element.
    size_t size = string_array->size();
    for (size_t i{0}; i < size; ++i)
    {
        for (int j{string_array->at(i).length()-1}; j>=0; --j)
        {
            if (string_array->at(i).at(j) == " ")
            {
                string_array->at(i).remove(j,1);
            }
        }
    }

    for (size_t i{size}; i>0; --i)
    {
        this->insertPlainText(string_array->at(i-1));
        this->insertPlainText("\n");
    }

    delete string_array;
    string_array = nullptr;
}


void Editor::sortSelection()
{
    QTextCursor cursor = this->textCursor();
    if (cursor.hasSelection())
    {
        /* Get selected text from the editor */
        QTextDocumentFragment selection = cursor.selection();
        /* Create a string list of selected text */
        QStringList list = selection.toPlainText().split("\n");
        /* Sort list and insert */
        list.sort();
        for (int i{0}; i<list.size(); ++i)
        {
            this->insertPlainText(list.at(i));
            if (i != list.size()-1)
                this->insertPlainText("\n");
        }
    }
}


void Editor::resizeEvent(QResizeEvent *e)
{
    QPlainTextEdit::resizeEvent(e);

    QRect cr = contentsRect();
    lineNumberArea->setGeometry(QRect(cr.left(),
                                      cr.top(),
                                      lineNumberAreaWidth(),
                                      cr.height()));
}

#ifdef Q_OS_WIN
void Editor::dropEvent(QDropEvent *e)
{
    QList<QUrl> ql = e->mimeData()->urls();
    QString s = ql.at(0).path();
    if (!s.isEmpty())
    {
        s.remove(0,1);
        this->fileNew();
        this->fileOpen(s);
    }
}
#endif

void Editor::lineNumberAreaPaintEvent(QPaintEvent *event)
{
    QPainter painter(lineNumberArea);
    //painter.fillRect(event->rect(), QColor(34,34,34));
    painter.fillRect(event->rect(), QColor(211,211,211,25));

    QTextBlock block = firstVisibleBlock();
    int blockNumber = block.blockNumber();
    int top = static_cast<int>
            (blockBoundingGeometry(block).translated(contentOffset()).top());
    int bottom = top + static_cast<int>(blockBoundingRect(block).height());

    while (block.isValid() && top <= event->rect().bottom())
    {
        if (block.isVisible() && bottom >= event->rect().top()) {
            QString number = QString::number(blockNumber + 1);
            painter.setPen(Qt::darkGray);
            painter.drawText(0, top, lineNumberArea->width(),
                             fontMetrics().height(),
                             //Qt::AlignRight, number);
                             Qt::AlignCenter, number);
        }

        block = block.next();
        top = bottom;
        bottom = top + static_cast<int>(blockBoundingRect(block).height());
        ++blockNumber;
    }
}
