/*
 *	fileexplorer.h
 *  This file is part of NotepadQt
 *
 *  Copyright (C) 2019, Stian Halstensen
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef FILEEXPLORER_H
#define FILEEXPLORER_H

#include <QDir>
#include <QListWidget>

class Editor;
class QListWidgetItem;

class FileExplorer : public QListWidget
{
    Q_OBJECT

public:
    FileExplorer();
    void toggleExplorer(Editor *e = nullptr, const QString &s = "./");
    void itemSelect();

private:
    void refreshList();
    Editor *editor { nullptr };
    QDir curDir;

private slots:
    void itemClicked(QListWidgetItem*); /* mouse click LMB */
};

#endif // FILEEXPLORER_H
