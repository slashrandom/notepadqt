# NotepadQt
NotepadQt is a free (free as in both "free speech" and "free beer") cross-platform text- and (eventually) code editor.
Design goals: Lightweight, maximum ease of customization, and efficient.

## Framework
NotepadQt is built using the QT5 framework.

## License 
GNU General Public License v2.0

   